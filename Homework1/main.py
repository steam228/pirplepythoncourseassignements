"""
This is my favorite song by Jeff Buckley
"""

artist = "Jeff Buckley"
genre = "Buckley"
name = "Grace"
duration = "5:22" # no need for specific time besides a string
durationInSeconds = 322

print(artist)
print(genre)
print(name)
print(duration)
print(durationInSeconds)
