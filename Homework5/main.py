"""""
homework #5
Python is Easy

"""""

for x in range(1,101):
    for y in range(2,x):
        if x%y==0:
            break
    else:
        print("prime")
        continue
    if x%3==0 and x%5==0:
        print("FizzBuzz")
        continue
    if x%3==0:
        print("Fizz")
        continue
    if x%5==0:
        print("Buzz")
        continue
    print(x)
