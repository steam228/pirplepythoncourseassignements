
"""""
homework #6
Python is Easy

"""""

screenLimit = 80


def drawTable(rows):
    columns = rows+1
    if rows < screenLimit:
        for i in range(rows):
            if i%2==0:
                for j in range(columns):
                    if j%2==0:
                        if j!= columns-1:
                            print("|" ,end="")
                        else:
                            print("|")

                    else:
                        if j != columns-1:
                            print(" " ,end="")
                        else:
                            print(" ")
            else:
                print("-"*columns)
        return(True)
    else:
        return(False)


print(drawTable(40))
