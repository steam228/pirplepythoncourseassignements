"""""
homework #4
Python is Easy

"""""

counter = 0
myUniqueList = []
myLeftovers = []
success = False


def addToList(value):
    equal = False
    for elem in myUniqueList:
        if value == elem:
            equal = True
    if equal != True:
        myUniqueList.append(value)
        return True
    else:
        myLeftovers.append(value)
        return False


success = addToList(34)
print(success)
success = addToList(34)
print(success)
success = addToList("Praia")
print(success)
success = addToList(22)
print(success)
success = addToList(3411)
print(success)
success = addToList("Praia")
print(success)
success = addToList("France")
print(success)

print(myUniqueList)
print(myLeftovers)
